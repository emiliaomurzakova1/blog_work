from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)


api_urlpatterns = [
    path('', include('apps.cars.api.urls')),
    path("", include("apps.posts.api.urls")),
    path("", include("apps.teams.api.urls")),
    path("", include('apps.users.api.urls')),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.users.urls')),
    path("team/", include("apps.teams.urls")),
    path("cars/", include("apps.cars.urls")),
    path('post/', include('apps.posts.urls')),
    path('api/', include(api_urlpatterns)),
    # path('client/', include('apps.client.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)